<?php
require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__File__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addGlobal('ma_valeur', "Hello There!");
    $twig->addFilter(new Twig_Filter('trad', function ($string) {
        return $string;
    }));
});

Flight::route('/first_view/', function () {
    $data = [
        'contenu' => 'Hello World!',
        'name' => 'Ben Kenobi',
    ];
    Flight::view()->display('first_view.twig', $data);
});

Flight::route('/second_view/',function(){
    $data =[
        'Personnage' => 'Bowser'
    ];
    Flight::view()->display('second_view.twig',$data);
});

Flight::route('/', function () {
    echo 'Hello World!';
    echo '<p><a href ="/first_view">1stview </a></p>';
    echo '<p><a href ="/second_view">2ndview </a></p>';
});

Flight::route('/hello/@name', function ($name) {
    echo "<h2>Hello, $name!</h2>";
});

Flight::start();