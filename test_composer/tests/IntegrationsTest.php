<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest
{

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Hello World!", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
    public function test_hello()
    {
        $name = "RaphTG";
        $response = $this->make_request("GET", "/hello/$name");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("<h2>Hello, $name!</h2>", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
   
    public function test_view()
    {
        $response = $this ->make_request("GET", "/first_view");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
        $this->assertContains("Hello There!", $response->getBody()->getContents());
    }

    public function test_secondview()
    {
        $response = $this ->make_request("GET", "/second_view");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
        $this->assertContains("Bowser", $response->getBody()->getContents());
    }
}